# Video Sitemap Generator
To use the Video Sitemap Generator, python must first be installed on your machine.

If you are not working from the repository, download the sitemap.py and videos.json files to the same directory on your computer.

In the terminal, navigate to the directory containing the files.

In the terminal, type the following command:

`python sitemap.py`

This will create a file in the same directory named videoSitemap.xml

The file is not very human readable, but it should be fine for GoogleBot's consumption.

However, I do use the [Atom Beautify](https://atom.io/packages/atom-beautify) to clean up the file.

**************************

Upload the created file to the production server and submit the updated sitemap to the [Google Search Console](https://search.google.com/search-console/sitemaps?resource_id=https%3A%2F%2Fwww.pcom.edu%2F&hl=en)

**************************
The [Simple JSON Video Scheme Generator](https://docs.google.com/spreadsheets/d/1eLbtgZwGLCGUjMCvHRzieoHffuRptL4Q6dRi5F9IgZM/edit#gid=0) created by Seer is used to add videos to the videos.json file.
Use the generator to generate the structured data for a video and add to the end of the existing array (comma separated).
Add a 'loc' key with the value of where the video is hosted on the PCOM website.
