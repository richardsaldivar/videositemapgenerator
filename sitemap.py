# -*- coding: utf-8 -*-
import json
import sys

input_file = open ('videos.json')
json_array = json.load(input_file)

# https://stackoverflow.com/questions/16742381/how-to-convert-youtube-api-duration-to-seconds/16743442
# convert youtube api duration format to seconds
def ytDurationToSeconds(duration): #eg P1W2DT6H21M32S
    week = 0
    day  = 0
    hour = 0
    min  = 0
    sec  = 0

    duration = duration.lower()

    value = ''
    for c in duration:
        if c.isdigit():
            value += c
            continue

        elif c == 'p':
            pass
        elif c == 't':
            pass
        elif c == 'w':
            week = int(value) * 604800
        elif c == 'd':
            day = int(value)  * 86400
        elif c == 'h':
            hour = int(value) * 3600
        elif c == 'm':
            min = int(value)  * 60
        elif c == 's':
            sec = int(value)

        value = ''

    return week + day + hour + min + sec

def createSiteMap(json_array):
    sitemap = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">'
    for element in json_array:
        duration = ytDurationToSeconds(format(element["duration"]).encode('utf-8'))
        videoElement = '<url>'
        videoElement += '<loc>' + format(element["loc"]).encode('utf-8') + '</loc>'
        videoElement += '<video:video>'
        videoElement += '<video:thumbnail_loc>' + format(element["thumbnailUrl"]).encode('utf-8') + '</video:thumbnail_loc>'
        videoElement += '<video:title>' + format(element["name"]).encode('utf-8') + '</video:title>'
        videoElement += '<video:description>' + format(element["description"]).encode('utf-8') + '</video:description>'
        videoElement += '<video:player_loc>' + format(element["embedUrl"]) .encode('utf-8')+ '</video:player_loc>'
        videoElement += '<video:duration>' + str(duration) + '</video:duration>'
        videoElement += '<video:publication_date>' + format(element["uploadDate"]).encode('utf-8') + '</video:publication_date>'
        videoElement += '<video:family_friendly>yes</video:family_friendly>'
        videoElement += '<video:requires_subscription>no</video:requires_subscription>'
        videoElement += '<video:uploader info="https://www.pcom.edu/">PCOM</video:uploader>'
        videoElement += '<video:live>no</video:live>'
        videoElement += '</video:video>'
        videoElement += '</url>'
        sitemap += videoElement
    sitemap += '</urlset>'
    return sitemap

sys.stdout=open("videoSitemap.xml","w")
print (createSiteMap(json_array))
sys.stdout.close()
